import styled from 'styled-components/native'
import { View, Animated } from 'react-native'

export const SpaceBackground = styled(View)`
  width: 100%;
  height: 100%;
  position: absolute;
`

export const Star = styled(Animated.View)`
  position: absolute;
  top: 0;
  left: ${props => props.left};
  background-color: rgba(255, 255, 255, 0.9);
  width: 1;
  height: 1;
`

export const SpaceLayer = styled(View)`
  height: 100%;
  width: 100%;
  background-color: #030016;
  position: absolute;
  top: 0;
  left: 0;
`