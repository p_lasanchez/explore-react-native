import React from 'react'
import { View } from 'react-native'
import styled from 'styled-components/native'
import { LinkText } from '../../Common/styled'

const TimeView = styled(View)`
  align-items: center;
  justify-content: center;
`

interface TimeTextProps {
  value: string
}
export const TimeText = ({ value }: TimeTextProps): JSX.Element => (
  <TimeView>
    <LinkText>{value}</LinkText>
  </TimeView>
)
