/* eslint-disable import/no-extraneous-dependencies */
import styled from 'styled-components/native'
import { View, Text, ImageBackground } from 'react-native'

export const Wrapper = styled(View)`
  background-color: ${props => props.theme.colors.blue};
  flex: 1;
`

export const InnerWrapper = styled(View)`
  padding-left: 20;
  padding-right: 20;
  padding-top: 30;
  height: 100%;
`

interface BulletProps {
  isTouched?: boolean
}
export const Bullet = styled(View)<BulletProps>`
  background-color: ${props => props.theme.colors.orange};
  border-radius: 20;
  width: 20;
  height: 20;
  margin-right: 10;
  position: absolute;
  opacity: ${props => props.isTouched ? 0.4 : 1};
`

export const LinkView = styled(View)`
  margin-bottom: 20;
`

interface LinkTextProps {
  padLeft?: boolean;
}
export const LinkText = styled(Text)<LinkTextProps>`
  color: ${props => props.theme.colors.white};
  font-size: 16;
  padding-left: ${props => props.padLeft ? 30 : 0};
`

export const AppImageBackground = styled(ImageBackground)`
  height: 100%;
  width: 100%;
  position: absolute;
`
