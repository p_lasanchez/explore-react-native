import React, { useState, useEffect } from 'react'
import { Animated, Dimensions } from 'react-native'
import { Star } from './styled'

interface StarLayerModel {
  left: number
  duration: number
}
export const StarLayer = React.memo(({ left, duration }: StarLayerModel) => {
  const [top, setTop] = useState(new Animated.Value(0))

  useEffect(() => {
    Animated.timing(top, {
      duration: 5000 * duration,
      toValue: Dimensions.get('window').height,
      useNativeDriver: true
    }).start(
      () => {
        setTop(new Animated.Value(0))
      }
    )
  }, [top, duration])

  return (
    <Star
      left={left}
      style={{ transform: [{ translateY: top }] }}
    />
  )
})