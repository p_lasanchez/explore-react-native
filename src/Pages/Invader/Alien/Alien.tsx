/* eslint-disable global-require */
import React from 'react'
import SvgUri from 'expo-svg-uri'

export const AlienSvg = React.memo(() => (
  <SvgUri
    width="40"
    height="40"
    source={require('../../../../assets/invader/ufo-02.svg')}
    fill="white"
    fillAll={true}
  />
))
