import styled from 'styled-components/native'
import { Animated } from 'react-native'

export interface BulletShapeProps {
  color: string
  top: number
  left: number
}
export const BulletShape = styled(Animated.View)`
  width: 6;
  height: 10;
  border-radius: 5;
  background-color: ${props => props.color};
  position: absolute;
  left: ${props => props.left};
  top: ${props => props.top};
`
