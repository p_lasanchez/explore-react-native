import React from 'react'
import { Provider } from 'react-redux'
import { ThemeProvider } from 'styled-components/native'
import { theme } from './styled/theme'
import { Main } from './src/Main'
import { store } from './src/Store'

const App = () => (
  <Provider store={store}>
    <ThemeProvider theme={theme}>
      <Main />
    </ThemeProvider>
  </Provider>
)

export default App
