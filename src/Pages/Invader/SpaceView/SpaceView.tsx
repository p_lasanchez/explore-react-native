import React, { useEffect, useState, memo } from 'react'
import { StarModel } from './models'

import { SpaceLayer } from './styled'
import { StarLayer } from './StarLayer'
import { getStars } from './services'

export const SpaceView = memo(() => {
  const [stars, setStars] = useState<StarModel[]>([])

  useEffect(() => {
    setStars(getStars())
  }, [])

  return (
    <SpaceLayer>
      {
        stars.map((star: StarModel) => (
          <StarLayer key={star.id} left={star.left} duration={star.duration} />
        ))
      }
    </SpaceLayer>
  )
})
