import React from 'react'
import { Metro } from './Metro'
import { MetroModel } from './service'
import { MetroImage } from './MetroImage'

interface MetroListType {
  metros: MetroModel[]
  tracksViewChanges: boolean
}
export const MetroList = ({ metros, tracksViewChanges }: MetroListType): JSX.Element => (
  <>
    {metros.map((metro: MetroModel) => (
      <Metro
        key={metro.recordid}
        coordinates={metro.coordinates}
        title={metro.title}
        line={metro.line}
        tracksViewChanges={tracksViewChanges}
      >
        <MetroImage />
      </Metro>
    ))}
  </>
)
