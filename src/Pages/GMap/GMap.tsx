import React, { useState, useEffect } from 'react'
import MapView, { Region } from 'react-native-maps'
import { NavigationScreenComponent } from 'react-navigation'
import { Asset } from 'expo-asset'

import { getLocation, getMetros, MetroModel } from './service'
import { MetroList } from './MetroList'
import { lines } from './MetroImage'

export const GMap: NavigationScreenComponent<{}, {}> = ((): JSX.Element => {
  const [location, setLocation] = useState<Region>(getLocation())
  const [metros, setMetros] = useState<MetroModel[]>([])
  const [tracksViewChanges, setTracksViewChanges] = useState(true)

  useEffect(() => {
    Object.values(lines).forEach(line => {
      Asset.fromModule(line as string).downloadAsync()
    })
  })

  useEffect(() => {
    navigator.geolocation.getCurrentPosition(position => {
      const { latitude, longitude } = position.coords
      setLocation(getLocation({ latitude, longitude }))
    })
  }, [])

  useEffect(() => {
    setMetros(getMetros())
  }, [])

  const onMapReady = () => {
    setTracksViewChanges(false)
  }

  return <MapView
    style={{flex: 1}}
    region={location}
    showsUserLocation={true}
    onMapReady={onMapReady}
  >
    <MetroList metros={metros} tracksViewChanges={tracksViewChanges} />
  </MapView>
})
GMap.navigationOptions = {
  title: 'Where Am I'
}
