import { Dimensions } from 'react-native'
import { Region } from 'react-native-maps'
import { metros } from './metros'

interface CoordsModel {
  latitude: number
  longitude: number
}
export const getLocation = (
  { latitude, longitude }: CoordsModel = { latitude: 0, longitude: 0 }
): Region => {
  const { width, height } = Dimensions.get('window')
  const ASPECT_RATIO = width / height
  const latitudeDelta = 0.0922
  const longitudeDelta = latitudeDelta * ASPECT_RATIO
  return {
    latitude,
    longitude,
    latitudeDelta,
    longitudeDelta
  }
}

export interface MetroModel {
  recordid: string
  coordinates: number[]
  title: string
  line: string
}
export const getMetros = (): MetroModel[] => (
  metros.map(metro => ({
    coordinates: metro.geometry.coordinates,
    title: metro.fields.nom_iv,
    recordid: metro.recordid,
    line: metro.fields.ligne
  }))
)