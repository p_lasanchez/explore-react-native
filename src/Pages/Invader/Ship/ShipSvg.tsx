/* eslint-disable global-require */
import React from 'react'
import SvgUri from 'expo-svg-uri'

export const ShipSvg = React.memo(() => (
  <SvgUri
    width="50"
    height="50"
    source={require('../../../../assets/invader/spaceship.svg')}
    fill="white"
    fillAll={true}
  />
))
