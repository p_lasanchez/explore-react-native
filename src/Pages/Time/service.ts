import { useState, useEffect } from 'react'

const withLeadingZero = (value: number): string => {
  return value < 10 ? `0${value}` : String(value)
}

export interface FormatTimeModel {
  text: string
  hours: number
  minutes: number
  seconds: number
}
const formatTime = (): FormatTimeModel => {
  const date: Date = new Date()
  const hours: number = date.getHours()
  const minutes: number = date.getMinutes()
  const seconds: number = date.getSeconds()
  return {
    text: `${withLeadingZero(hours)} : ${withLeadingZero(minutes)} : ${withLeadingZero(seconds)}`,
    hours,
    minutes,
    seconds
  }
}

export const useLiveTime = (): FormatTimeModel => {
  const [time, setTime] = useState<FormatTimeModel>(formatTime())

  useEffect(() => {
    const interval = setInterval(() => {
      setTime(formatTime())
    }, 1000)

    return () => clearInterval(interval)
  }, [])

  return time
}

export interface RotateStyleModel {
  transform: { rotate: string }[]
}
export const rotateStyle = (value: number): RotateStyleModel => ({
  transform: [{ rotate: `${value}deg` }]
})
