import { createStore, combineReducers } from 'redux'
import { shipReducer, defaultShipState, ShipState } from './Invader/ship.reducer'

export interface RootState {
  shipReducer: ShipState
}

export const defaultRootState: RootState = {
  shipReducer: defaultShipState,
}
const rootReducer = combineReducers<RootState>({
  shipReducer,
})

export const store = createStore(
  rootReducer,
  defaultRootState
)
