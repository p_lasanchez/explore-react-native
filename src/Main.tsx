import { createAppContainer } from 'react-navigation'
import { createStackNavigator } from 'react-navigation-stack'
import { theme } from '../styled/theme'

import { Home } from './Pages/Home/Home'
import { GMap } from './Pages/GMap/GMap'
import { Contacts } from './Pages/Contacts/Contacts'
import { BlankPage } from './Pages/BlankPage'
import { Time } from './Pages/Time/Time'
import { Invader } from './Pages/Invader'

const MainNavigator = createStackNavigator({
  Home: { screen: Home },
  GMap: { screen: GMap },
  Contacts: { screen: Contacts },
  Time: { screen: Time },
  Invader: { screen: Invader },
  BlankPage: { screen: BlankPage }
},
{
  initialRouteName: 'Home',
  defaultNavigationOptions: {
    headerStyle: {
      backgroundColor: theme.colors.orange,
    },
    headerTintColor: theme.colors.white,
    headerTitleStyle: {
      fontWeight: 'normal',
    },
  },
})

export const Main = createAppContainer(MainNavigator)
