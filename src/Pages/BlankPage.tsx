import React from 'react'
import { NavigationScreenComponent } from 'react-navigation'
import { View } from 'react-native'
import { LinkText } from '../Common/styled'
import { withWrapper } from '../Common/withWrapper'

export const BlankPage: NavigationScreenComponent<{}, {}> = withWrapper((): JSX.Element => {
  return (
    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
      <LinkText>Blank Page</LinkText>
    </View>
  )
})
BlankPage.navigationOptions = {
  title: 'Blank Page'
}
