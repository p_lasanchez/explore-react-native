import React from 'react'
import { rotateStyle, RotateStyleModel } from './service'
import { BorderView, NeedleHours, NeedleInside, NeedleMinutes, NeedleSeconds } from './styledWatch'

interface WatchProps {
  hours: number
  minutes: number
  seconds: number
}
export const Watch = ({ hours, minutes, seconds }: WatchProps): JSX.Element => {

  const hoursRotation: RotateStyleModel = rotateStyle((360 / 12) * hours)
  const minutesRotation: RotateStyleModel = rotateStyle((360 / 60) * minutes)
  const secondsRotation: RotateStyleModel = rotateStyle((360 / 60) * seconds)

  return (
    <BorderView>
      <NeedleHours style={hoursRotation}>
        <NeedleInside />
      </NeedleHours>
      <NeedleMinutes style={minutesRotation}>
        <NeedleInside />
      </NeedleMinutes>
      <NeedleSeconds style={secondsRotation}>
        <NeedleInside orange />
      </NeedleSeconds>
    </BorderView>
  )
}