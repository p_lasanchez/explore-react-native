import React from 'react'
import TestRenderer from 'react-test-renderer'
import { ThemeProvider } from 'styled-components/native'

import { Watch } from './Watch'
import { NeedleHours, NeedleMinutes, NeedleSeconds } from './styledWatch'
import { theme } from '../../../styled/theme'

describe('<Watch />', () => {
  let renderer: TestRenderer.ReactTestRenderer

  beforeEach(() => {
    renderer = TestRenderer.create(
      <ThemeProvider theme={theme}>
        <Watch hours={10} minutes={30} seconds={4} />
      </ThemeProvider>
    )
  })

  afterEach(() => {
    renderer.unmount()
  })

  it('should render Watch', () => {
    const instance = renderer.root
    const elH = instance.findByType(NeedleHours)
    const elM = instance.findByType(NeedleMinutes)
    const elS = instance.findByType(NeedleSeconds)

    expect(elH.props.style).toEqual({
      transform: [{ rotate: '300deg' }]
    })
    expect(elM.props.style).toEqual({
      transform: [{ rotate: '180deg' }]
    })
    expect(elS.props.style).toEqual({
      transform: [{ rotate: '24deg' }]
    })
  })
})