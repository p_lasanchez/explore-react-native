type ColorList = 'black' | 'white' | 'orange' | 'yellow' | 'green' | 'blue'

type Colors = {
  [key in ColorList]: string
}

export interface Theme {
  colors: Colors
}

export const theme: Theme = {
  colors: {
    black: '#000',
    white: '#FFF',
    orange: '#d35b3f',
    yellow: '#ded797',
    blue: '#31595e',
    green: '#b0c38d',
  }
}