/* eslint-disable no-plusplus */
import { Dimensions } from 'react-native'
import { StarModel } from './models'

export const getStars = () => {
  const stars: StarModel[] = []
  for (let i = 100; i > 0; i--) {
    stars.push({
      id: i,
      left: Math.ceil(Math.random() * Dimensions.get('window').width),
      duration: Math.random()
    })
  }
  return stars
}