import React, { useState, useLayoutEffect } from 'react'
import styled from 'styled-components/native'
import { Text, View, Animated } from 'react-native'
import { MetroImage } from './MetroImage'

const ParentView = styled(View)`
  shadow-opacity: 0.30;
  shadow-radius: 5;
  shadow-color: ${props => props.theme.colors.black};
  shadow-offset: 0 0;
`
const ModalStyle = styled(Animated.View)`
  position: absolute;
  top: -4;
  left: 25;
  background-color: ${props => props.theme.colors.white};
  padding-left: 10;
  padding-right: 10;
  padding-top: 5;
  padding-bottom: 5;
  border-radius: 5;
  overflow: hidden;
  z-index: 10;
`

interface ViewModalType {
  title: string
  line: string
}
export const ViewModal = ({ title, line }: ViewModalType): JSX.Element => {
  const [size] = useState<Animated.Value>(new Animated.Value(0))
  const toValue = 150
  const duration = 150

  useLayoutEffect(() => {
    Animated.timing(
      size,
      { toValue, duration }
    ).start()
  }, [size])

  return (
    <ParentView>
      <ModalStyle style={{ width: size }}>
        <View style={{ width: toValue - 20 }}>
          <Text style={{ alignSelf: 'stretch', marginBottom: 5 }}>{title}</Text>
        </View>
        <MetroImage line={line} />
      </ModalStyle>
    </ParentView>
  )
}