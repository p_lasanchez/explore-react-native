import styled from 'styled-components/native'
import { Text } from 'react-native'

export const Debug = styled(Text)`
  color: white;
  position: absolute;
  top: 0;
  left: 0;
  z-index: 10;
`
