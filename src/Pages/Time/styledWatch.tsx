import { View } from 'react-native'
import styled from 'styled-components/native'

export const BorderView = styled(View)`
  border-style: solid;
  border-color: ${props => props.theme.colors.white};
  border-width: 4;
  border-radius: 200;
  width: 200;
  height: 200;
`

export const NeedleSeconds = styled(View)`
  width: 2;
  height: 188;
  position: absolute;
  left: 95;
  top: 2;
`
export const NeedleMinutes = styled(View)`
  width: 4;
  height: 160;
  position: absolute;
  left: 94;
  top: 16;
`
export const NeedleHours = styled(View)`
  width: 4;
  height: 120;
  position: absolute;
  left: 94;
  top: 36;
`
interface NeedleInsideProps {
  orange?: boolean
}
export const NeedleInside = styled(View)<NeedleInsideProps>`
  height: 50%;
  background-color: ${
    props => props.orange ? props.theme.colors.orange : props.theme.colors.white
  };
  border-radius: 8;
`
