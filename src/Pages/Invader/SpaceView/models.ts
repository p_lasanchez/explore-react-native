export interface StarModel {
  id: number
  left: number,
  duration: number
}