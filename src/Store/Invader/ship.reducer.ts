import { Dimensions } from 'react-native'
import { SHIP, ShipAction, InitializeShipAction } from './ship.actions'

const SHIP_WIDTH = 50

export interface ShipState {
  startShipMove: number
  nextShipMove: number
  currentShipMove: number
}

const screenWidth = Dimensions.get('window').width
const initialPosition = Math.round(screenWidth / 2 - (SHIP_WIDTH / 2))

export const defaultShipState: ShipState = {
  startShipMove: 0,
  nextShipMove: initialPosition,
  currentShipMove: initialPosition
}

type Action = ShipAction | InitializeShipAction

export const shipReducer = (state: ShipState = defaultShipState, action: Action): ShipState => {
  let x: number

  switch  (action.type) {
    case SHIP.INITIALIZE:
      return defaultShipState

    case SHIP.SET_POSITION:
      x = action.x
      if (x < 0) { x = 0 }
      if (x > screenWidth - SHIP_WIDTH) { x = screenWidth - SHIP_WIDTH }
      return {
        ...state,
        currentShipMove: x
      }

    case SHIP.SET_START_SHIP_MOVE:
      x = action.x
      return { ...state, startShipMove: x }

    case SHIP.SET_NEXT_SHIP_MOVE:
      x = action.x
      return { ...state, nextShipMove: x }

    default:
      return state
  }
}
