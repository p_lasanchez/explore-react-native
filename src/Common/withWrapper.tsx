/* eslint-disable global-require */
import React from 'react'
import { StatusBar } from 'react-native'

import { Wrapper, AppImageBackground, InnerWrapper } from './styled'

export const withWrapper = <P, >(Component: React.FunctionComponent): React.FunctionComponent<P> => {
  const FunctionWrapper = (props: P): JSX.Element => (
    <Wrapper>
      <StatusBar barStyle="light-content" />
      <AppImageBackground
        imageStyle={{ opacity: 0.4 }}
        source={require('../../assets/mobileswall.jpg')}
      />
      <InnerWrapper>
        <Component {...props} />
      </InnerWrapper>
    </Wrapper>
  )

  return FunctionWrapper
}
