import { RootState } from '..'

export const getNextShipMove = (state: RootState): number => state.shipReducer.nextShipMove
export const getStartShipMove = (state: RootState): number => state.shipReducer.startShipMove
export const getCurrentShipMove = (state: RootState): number => state.shipReducer.currentShipMove
