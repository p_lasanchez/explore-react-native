export enum SHIP {
  INITIALIZE = 'SHIP/INTIALIZE',
  SET_POSITION = 'SHIP/SET_POSITION',
  SET_START_SHIP_MOVE  = 'SHIP/SET_START_SHIP_MOVE',
  SET_NEXT_SHIP_MOVE  = 'SHIP/SET_NEXT_SHIP_MOVE'
}

export interface InitializeShipAction {
  type: SHIP.INITIALIZE
}

export interface ShipAction {
  type: SHIP.SET_NEXT_SHIP_MOVE | SHIP.SET_START_SHIP_MOVE | SHIP.SET_POSITION
  x: number
}

export const initializeShipAction = (): InitializeShipAction => ({
  type: SHIP.INITIALIZE
})

export const setShipPositionAction = (x: number): ShipAction => ({
  type: SHIP.SET_POSITION,
  x
})

export const setStartShipMoveAction = (x: number): ShipAction => ({
  type: SHIP.SET_START_SHIP_MOVE,
  x
})

export const setNextShipMoveAction = (x: number): ShipAction => ({
  type: SHIP.SET_NEXT_SHIP_MOVE,
  x
})
