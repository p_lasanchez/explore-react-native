import React, { useState, useEffect } from 'react'
import { NavigationScreenComponent } from 'react-navigation'
import { FlatList, ListRenderItem } from 'react-native'
import * as ContactLib from 'expo-contacts'

import { withWrapper } from '../../Common/withWrapper'
import { ContactItem } from './ContactItem'

export const Contacts: NavigationScreenComponent<{}, {}> = withWrapper((): JSX.Element => {
  const [contacts, setContacts] = useState<ContactLib.Contact[]>([])
  const keyExtractor = (contact: ContactLib.Contact) => contact.id
  const renderItem: ListRenderItem<ContactLib.Contact> = ({ item }: { item: ContactLib.Contact }) => (
    <ContactItem
      firstName={item.firstName}
      lastName={item.lastName}
      phoneNumbers={item.phoneNumbers}
    />
  )

  useEffect(() => {
    ContactLib.getContactsAsync({
      fields: [
        ContactLib.Fields.FirstName,
        ContactLib.Fields.LastName,
        ContactLib.Fields.PhoneNumbers
      ],
    }).then((result: ContactLib.ContactResponse) => {
      setContacts(result.data)
    })
  }, [])
  return (
    <FlatList<ContactLib.Contact>
      data={contacts}
      keyExtractor={keyExtractor}
      renderItem={renderItem}
    />
  )
})
Contacts.navigationOptions = {
  title: 'Contacts'
}