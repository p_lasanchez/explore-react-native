import React from 'react'
import { ShipView } from './styled'
import { ShipSvg } from './ShipSvg'

interface ShipProps {
  x: number
}
export const Ship = React.memo(({ x }: ShipProps): JSX.Element => (
  <ShipView
    style={{ transform: [{ translateX: x }] }}
  >
    <ShipSvg />
  </ShipView>
))
