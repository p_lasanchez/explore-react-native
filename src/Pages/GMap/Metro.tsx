import React, { useState, memo } from 'react'
import { Marker, LatLng } from 'react-native-maps'
import { ViewModal } from './ViewModal'

interface MetroType {
  coordinates: number[]
  title: string
  line: string
  tracksViewChanges: boolean
  children: JSX.Element
}
export const Metro = memo(({ coordinates, title, line, tracksViewChanges, children }: MetroType): JSX.Element => {
  const [isVisible, setVisible] = useState(false)
  const [ longitude, latitude ] = coordinates
  const coords: LatLng = {
    latitude,
    longitude
  }
  const onPress = () => {
    requestAnimationFrame(() => {
      setVisible(!isVisible)
    })
  }
  return (
    <Marker coordinate={coords} onPress={onPress} tracksViewChanges={tracksViewChanges}>
      {children}
      {isVisible && <ViewModal title={title} line={line} />}
    </Marker>
  )
})
