import React, { useEffect, memo, useState, useMemo } from 'react'
import { Animated, Dimensions, LayoutChangeEvent, Easing } from 'react-native'
import { BulletShape } from './styled'

interface BulletProps {
  color: string
  left: number
}

export const Bullet = memo(({ color, left }: BulletProps) => {
  const direction = useMemo<number>(() => ( Dimensions.get('window').height * -1 + 224 ), [])
  const top = useMemo<number>(() => ( Dimensions.get('window').height - 224 ), [])

  const [animated, setAnimated] = useState(new Animated.Value(0))
  const [capturedPosition, setCapturedPosition] = useState(left)

  useEffect(() => {
    Animated.timing(animated, {
      duration: 600,
      toValue: direction - 8,
      useNativeDriver: true,
      easing: Easing.out(Easing.sin)
    }).start(
      () => {
        setAnimated( new Animated.Value(0) )
      }
    )
  }, [animated])

  useEffect(() => {
    setCapturedPosition(left)
  }, [animated])

  return (
    <BulletShape
      color={color}
      top={top}
      left={capturedPosition + 22}
      style={{ transform: [{ translateY: animated }] }}
    />
  )
})
