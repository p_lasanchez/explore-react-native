export interface LinkModel {
  title: string
  link: string
}

export const links: LinkModel[] = [
  { title: 'Blank Page', link: 'BlankPage' },
  { title: 'Where Am I', link: 'GMap' },
  { title: 'Contacts', link: 'Contacts' },
  { title: 'What time is it ?', link: 'Time' },
  { title: 'Invader', link: 'Invader' },
]
