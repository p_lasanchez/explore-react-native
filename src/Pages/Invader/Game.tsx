import React, { useEffect } from 'react'
import {
  PanGestureHandler, PanGestureHandlerGestureEvent, State, PanGestureHandlerStateChangeEvent
} from 'react-native-gesture-handler'

import { useSelector, useDispatch } from 'react-redux'
import { RootState } from '../../Store'
import {
  setNextShipMoveAction, setStartShipMoveAction, setShipPositionAction, initializeShipAction
} from '../../Store/Invader/ship.actions'

import { Ship } from './Ship'
import { SpaceView } from './SpaceView'
import { SpaceBackground } from './SpaceView/styled'
import { Bullet } from './Bullet'
import { AlienSvg } from './Alien'

import { getCurrentShipMove, getNextShipMove, getStartShipMove } from '../../Store/Invader/ship.queries'

export const Game = (): JSX.Element => {
  const dispatch = useDispatch()

  const nextShipMove = useSelector<RootState, number>(getNextShipMove)
  const startShipMove = useSelector<RootState, number>(getStartShipMove)
  const currentShipMove = useSelector<RootState, number>(getCurrentShipMove)

  useEffect(() => {
    dispatch( initializeShipAction() )
  }, [])

  const move = (
    evt: PanGestureHandlerGestureEvent
  ): void => {
    const x = (evt.nativeEvent.x - startShipMove) + nextShipMove
    dispatch( setShipPositionAction(x) )
  }

  const handleChange = (
    evt: PanGestureHandlerStateChangeEvent
  ): void => {
    const { state } = evt.nativeEvent
    if (state === State.BEGAN) dispatch( setStartShipMoveAction(evt.nativeEvent.x) )
    if (state === State.END) dispatch( setNextShipMoveAction(currentShipMove) )
  }

  return (
    <PanGestureHandler
      onGestureEvent={move}
      onHandlerStateChange={handleChange}
    >
      <SpaceBackground>
        <SpaceView />
        <Ship x={currentShipMove} />
        <Bullet color="cyan" left={currentShipMove} />
        <AlienSvg />
      </SpaceBackground>
    </PanGestureHandler>
  )
}
