import React from 'react'
import { NavigationScreenComponent } from 'react-navigation'
import { withWrapper } from '../../Common/withWrapper'
import { Link } from '../../Common/Link'
import { LinkModel, links } from './service'

export const Home: NavigationScreenComponent<{}, {}> = withWrapper<{}>((): JSX.Element => (
  <>
    {
      links.map((link: LinkModel) => (
        <Link title={link.title} link={link.link} key={link.link} />
      ))
    }
  </>
))
Home.navigationOptions = {
  title: 'Home'
}
