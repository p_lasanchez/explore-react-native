import React from 'react'
import TestRenderer, { act } from 'react-test-renderer'
import { Text } from 'react-native'
import { useLiveTime, FormatTimeModel } from './service'

jest.useFakeTimers()

const FakeComponent = (): JSX.Element => {
  const time: FormatTimeModel = useLiveTime()
  return (
    <Text>{time.text}</Text>
  )
}

const spyFormatTime = (h: number, m: number, s: number) => {
  jest.spyOn(Date.prototype, 'getHours').mockImplementation(() => h)
  jest.spyOn(Date.prototype, 'getMinutes').mockImplementation(() => m)
  jest.spyOn(Date.prototype, 'getSeconds').mockImplementation(() => s)
}

describe('Time service', () => {
  let renderer: TestRenderer.ReactTestRenderer

  beforeEach(() => {
    spyFormatTime(10, 20, 3)
    act(() => {
      renderer = TestRenderer.create(
        <FakeComponent />
      )
    })
  })

  afterEach(() => {
    renderer.unmount()
    expect(clearInterval).toBeCalled()
  })

  it('should useLiveTime', () => {
    const instance: TestRenderer.ReactTestInstance = renderer.root
    const el: TestRenderer.ReactTestInstance = instance.findByType(Text)

    expect(setInterval).toBeCalled()
    expect(Date.prototype.getHours).toBeCalledTimes(1)
    expect(el.props.children).toBe('10 : 20 : 03')

    act(() => {
      spyFormatTime(11, 21, 8)
      jest.advanceTimersByTime(1000)
    })
    expect(el.props.children).toBe('11 : 21 : 08')
  })
})
