import React from 'react'
import { NavigationScreenComponent } from 'react-navigation'
import { Game } from './Game'

export const Invader: NavigationScreenComponent<{}, {}> = (): JSX.Element => (
  <Game />
)
