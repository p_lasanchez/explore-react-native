import React, { useState } from 'react'
import { Vibration } from 'react-native'
import { useNavigation } from 'react-navigation-hooks'
import { NavigationParams } from 'react-navigation'
import { Bullet, LinkView, LinkText } from './styled'

interface LinkProps {
  title: string
  link: string
}
export const Link = (
  { title, link }: LinkProps
): JSX.Element => {
  const navigation = useNavigation<NavigationParams>()
  const [isTouched, setTouched] = useState(false)

  const onResponderGrant = (): void => {
    setTouched(true)
    Vibration.vibrate(1000)
  }

  const onResponderRelease = (): void => {
    setTouched(false)
    Vibration.cancel()
    navigation.navigate(link)
  }

  return (
    <LinkView
      onResponderGrant={onResponderGrant}
      onResponderRelease={onResponderRelease}
      onStartShouldSetResponder={() => true}
    >
      <Bullet isTouched={isTouched} />
      <LinkText padLeft>{title}</LinkText>
    </LinkView>
  )
}
