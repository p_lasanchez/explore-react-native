import React from 'react'
import { View } from 'react-native'
import { PhoneNumber } from 'expo-contacts'

import { LinkText } from '../../Common/styled'

interface ContactItemType {
  firstName?: string
  lastName?: string
  phoneNumbers?: PhoneNumber[]
}
export const ContactItem = ({
  firstName, lastName, phoneNumbers
}: ContactItemType) => {


  const primaryPhone = phoneNumbers &&
    phoneNumbers.reduce((next, phone) => {
      if (phone.number && !next) {
        return phone.number
      }
      return next
    }, '')

  return <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-between', paddingBottom: 15}}>
    <LinkText>
      {firstName} {lastName}
    </LinkText>
    <LinkText>
      {primaryPhone}
    </LinkText>
  </View>
}
