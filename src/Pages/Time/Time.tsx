import React from 'react'
import { View } from 'react-native'
import { NavigationScreenComponent } from 'react-navigation'
import styled from 'styled-components/native'

import { withWrapper } from '../../Common/withWrapper'
import { useLiveTime, FormatTimeModel } from './service'
import { TimeText } from './TimeText'
import { Watch } from './Watch'

const TimeView = styled(View)`
  align-items: center;
  justify-content: space-around;
  height: 60%;
`

export const Time: NavigationScreenComponent<{}, {}> = withWrapper((): JSX.Element => {
  const time: FormatTimeModel = useLiveTime()

  return (
    <TimeView>
      <Watch hours={time.hours} minutes={time.minutes} seconds={time.seconds} />
      <TimeText value={time.text} />
    </TimeView>
  )
})
Time.navigationOptions = {
  title: 'What time is it ?'
}
