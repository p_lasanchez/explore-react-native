/* eslint-disable global-require */
import React, { memo, useState } from 'react'
import { Image, ImageSourcePropType, Animated } from 'react-native'

type LineModel = {
  [key: string]: ImageSourcePropType
}
export const lines: LineModel = {
  '1': require('../../../assets/metro/M_1.png'),
  '2': require('../../../assets/metro/M_2.png'),
  '3': require('../../../assets/metro/M_3.png'),
  '3b': require('../../../assets/metro/M_3bis.png'),
  '4': require('../../../assets/metro/M_4.png'),
  '5': require('../../../assets/metro/M_5.png'),
  '6': require('../../../assets/metro/M_6.png'),
  '7': require('../../../assets/metro/M_7.png'),
  '7b': require('../../../assets/metro/M_7bis.png'),
  '8': require('../../../assets/metro/M_8.png'),
  '9': require('../../../assets/metro/M_9.png'),
  '10': require('../../../assets/metro/M_10.png'),
  '11': require('../../../assets/metro/M_11.png'),
  '12': require('../../../assets/metro/M_12.png'),
  '13': require('../../../assets/metro/M_13.png'),
  '14': require('../../../assets/metro/M_14.png'),
  'metro': require('../../../assets/metro/L_M.png')
}
interface MetroImageType {
  line?: string
}
export const MetroImage = memo(({ line }: MetroImageType): JSX.Element => {
  const source: ImageSourcePropType = line ? lines[line] : lines.metro
  const [fadeAnim] = useState(new Animated.Value(0))

  const handleAnimation = () => {
    Animated.timing(
      fadeAnim,
      { toValue: 1, duration: 300, useNativeDriver: true }
    ).start()
  }

  return (
    <Animated.View style={{ opacity: fadeAnim }}>
      <Image
        source={source}
        style={{ width: 20, height: 20, zIndex: 1 }}
        onLoadEnd={handleAnimation}
      />
    </Animated.View>
  )
}, (prev: MetroImageType, next: MetroImageType) => prev.line === next.line)
